<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>定制服务</title>
        <style>
        </style>
        <link rel="stylesheet" href="../css/showPro.css">
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <link rel="stylesheet" href="/css/commen.css">
        <link rel="stylesheet" href="/css/customMade.css">
    </head>
    <body>
        <div>
            <jsp:include page="top.jsp"></jsp:include>
        </div>
        <div id="middle">
            <div class="title-text">高端定制</div>
            <div>定制流程</div>
            <div class="procedure">
                <c:forEach begin="0" var="i" end="${stepLoops-1}">
                    <div>
                        <c:forEach items="${steps}" var="step" begin="${i*3}" end="${i*3+2}">
                            <div>
                                <div>
                                    <div>
                                        <img src="${step.stepLogo}" height="100" width="100"/>
                                        <div>${step.stepName}</div>
                                    </div>
                                </div>
                                <div><span>${step.stepDetail}</span></div>
                            </div>
                        </c:forEach>
                    </div>
                </c:forEach>
                <div>
                    <div>
                        <p>注意事项</p>
                        <P>1.预约时请提供有效的联系方式和联系人姓名。</P>
                        <P>2.缴纳定金后还没制作坯衣时，若客户取
                            消订单，可以退回定金;一旦确认坯衣，
                            订单不能取消，定金不能退回。</P>
                        <P>3.因本产品是定制产品，一旦确认坯衣，不是质量问题，本店一律不退换。</P>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <jsp:include page="bottom.jsp"></jsp:include>
            <jsp:include page="load.jsp"></jsp:include>
        </div>
    </body>
</html>