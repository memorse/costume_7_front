<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>新闻动态</title>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <link rel="stylesheet" href="/css/commen.css">
        <link rel="stylesheet" href="/css/news.css">
    </head>
    <body>
        <jsp:include page="top.jsp"></jsp:include>
        <div id="main">
            <div class="title-text">
                <span>新闻动态</span>
                <span>designer introduction</span>
            </div>
            <div class="news">
                <div name="news-menu">
                    <c:forEach items="${newsTypes}" var="newsType">
                        <a href="${newsType.priKey}"><div>${newsType.type}</div></a>
                    </c:forEach>
                </div>
                <div name="content">
                    <div class="hotnews">
                        <div><img src="../images/w72s.jpg" height="134" width="197"/></div>
                        <div>
                            <p>${news[0].title}</p>
                            <p>${news[0].content}</p>
                            <p><fmt:formatDate value="${news[0].pubTime}" type="date"></fmt:formatDate> </p>
                        </div>
                    </div>
                    <div class="news-list">
                        <div>
                            <c:forEach var="news" items="${news}">
                                <div>
                                    <span>${news.title}</span>
                                    <span><fmt:formatDate value="${news.pubTime}" type="date"></fmt:formatDate></span>
                                </div>
                            </c:forEach>
                        </div>

<%--                        分页按钮--%>
                        <div>
                            <div>&lt;</div>
                            <div>1</div>
                            <div>2</div>
                            <div>3</div>
                            <div>4</div>
                            <div>5</div>
                            <div>&gt;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="bottom.jsp"></jsp:include>
        <jsp:include page="load.jsp"></jsp:include>
    </body>
</html>
