<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/19
  Time: 9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>注册</title>
        <script src="/js/jquery.js"></script>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <link rel="stylesheet" href="//at.alicdn.com/t/font_2052253_aechmj40rfq.css"/>
        <script src="/js/register.js"></script>
        <link rel="stylesheet" href="/css/register.css"></link>
    </head>
    <body>
        <div id="register_div">
            <div>
                <span>帐号:</span>
                <input type="text" id="user_account" >
                <tip id="account_tip">帐号必须由字母开头,6-16位字母和数字组成</tip>
                <tip id="account_enable_tip_no" style="color: red">该帐号已存在</tip>
                <tip id="account_enable_tip_yes" style="color: green">帐号可用！</tip>
            </div>
            <div>
                <span>密码:</span>
                <input type="password" id="user_password" >
                <tip id="password_tip">密码必须由6-16位字母和数字组成</tip>
            </div>
            <div>
                <span>确认密码:</span>
                <input type="password" id="confirm_password" >
                <tip id="confirm_password_tip">两次输入的密码不一致</tip>
            </div>
            <div>
                <span>昵称:</span>
                <input type="text" id="user_nickname" >
                <tip id="nickname_tip">昵称不能为空</tip>
            </div>
            <div>
                <span>姓名:</span>
                <input type="text" id="user_name" >
                <tip id="name_tip">姓名不能为空</tip>
            </div>
            <div>
                <span>管理员:</span>
                <input type="radio" checked="checked" value="0" name="is_admin" id="is_admin_no" ><label for="is_admin_no">否</label>
                <input type="radio" value="1" name="is_admin" id="is_admin_yes" ><label for="is_admin_yes">是</label>
            </div>
            <div>
                <span>状态:</span>
                <input type="radio" checked="checked" value="1" name="enable" id="enable_yes" ><label for="enable_yes">可用</label>
                <input type="radio" value="0" name="enable" id="enable_no" ><label for="enable_no">不可用</label>
            </div>
            <div>
                <span>电话:</span>
                <input type="text" id="user_phone" >
            </div>
            <div>
                <span>性别:</span>
                <input type="radio" checked="checked" value="男" name="gender" id="user_gender_male" ><label for="user_gender_male">男</label>
                <input type="radio" value="女" name="gender" id="user_gender_fmale" ><label for="user_gender_fmale">女</label>
            </div>
            <div><span>配偶:</span><input type="text" id="user_mate" ></div>
            <div><span>生日:</span><input type="date" id="user_birthday" ></div>
            <div>
                <span>爱好:</span>
                <input type="checkbox" name="hobby" id="user_hobby_playtball" value="打球" ><label for="user_hobby_playtball">打球</label>
                <input type="checkbox" name="hobby" id="user_hobby_lookbook" value="看书" ><label for="user_hobby_lookbook">看书</label>
                <input type="checkbox" name="hobby" id="user_hobby_music"  value="音乐"><label for="user_hobby_music">音乐</label>
                <input type="checkbox" name="hobby" id="user_hobby_other" value="其它" ><label for="user_hobby_other">其它</label>
            </div>
            <div><span>结婚纪念日:</span><input type="date" id="user_wedding_anniversary" ></div>
            <div>
                <span>验证码:</span>
                <input type="text" id="code">
                <img src="/getVerifyImage" alt="" onclick="javascipt:$(this).prop('src','/getVerifyImage?'+window.Date.now())">
                <tip id="code_tip">验证码错误！</tip>
            </div>
            <div>
                <div onclick="do_register()" name="registerBtn"><span>注册</span></div>
                <div onclick="toload()" name="goToLoad">已有帐号,去登陆</div>
            </div>

        </div>
    </body>
</html>
