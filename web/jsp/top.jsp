<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <link rel="stylesheet" href="/css/top.css">
    </head>
    <body>
        <div id="tour">

            <div id="banner">
                <%--            头部文字标语--%>
                ${company.ad}
            </div>
<%--    头部导航菜单--%>
            <ul>
                <a href="#"><li >${company.name}</li></a>
                <c:forEach items="${navigator}" var="nav">
                    <c:if test="${nav.level==1}">
                        <a href="${nav.target}"><li id="cName">${nav.name}</li></a>
                    </c:if>
                </c:forEach>
            </ul>
        </div>
    </body>
</html>