<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <link rel="stylesheet" href="/css/bottom.css">
    </head>
    <body>
        <div id="copy-right">
            <div>
                <div></div>
                <div>GLAMORNING</div>
                <div></div>
            </div>
            <div>
                <div>
                    <c:forEach items="${news}" begin="0" var="news" end="2" >
                        <p><span>${news.title}</span><span><fmt:formatDate value="${news.pubTime}" type="date"></fmt:formatDate></span></p>
                    </c:forEach>
                </div>
                <div>
                    <p>地址：${company.address}</p>
                    <p>电话：${company.phone}</p>
                    <p>邮箱：${company.email}</p>
                </div>
                <div>
                    <div></div>
                    <div>
                        <p>扫描二维码 关注我们</p>
                        <p>时刻更新精彩时尚资讯</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>