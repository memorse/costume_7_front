<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <link rel="stylesheet" href="/css/commen.css">
        <link rel="stylesheet" href="/css/about.css">
    </head>
    <body>
        <jsp:include page="top.jsp"></jsp:include>
        <div id="main">
            <div>
                <div class="title-text">
                    <span>关于我们</span>
                    <span>about us</span>
                </div>
                <jsp:include page="Glamorning.jsp"></jsp:include>
            </div>
            <div>
                <div class="title-text">
                    <span>设计师们</span>
                    <span>designer introduction</span>
                </div>
                <div class="designer">
                    <c:forEach items="${designers}" begin="0" end="3" var="designer">
                        <div>
                            <div><img src="${designer.photo}" height="500" width="750"/></div>
                            <div>
                                <p>设计师：${designer.name}</p>
                                <p>${designer.introduce}</p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>

            <div>
                <div class="title-text">
                    <span>线下门店</span>
                    <span>OFFLINE STORE</span>
                </div>
                <div class="offline-photo">
                    <div>
                        <div><img src="${offlineStores[0].photo}" height="500" width="750"/></div>
                        <div>
                            <c:forEach items="${offlineStores}" varStatus="status" var="offlinestore">
                                <c:set var="index" value="${status.index}"></c:set>
                                    <div><img src="${offlinestore.photo}" height="500" width="750"/></div>

                            </c:forEach>
                        </div>
                    </div>
                    <div>
                        <div><span>线下门店地址</span></div>
                        <div>
                            <c:forEach var="offlineStore" items="${offlineStores}">
                                <p>${offlineStore.address}</p>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <jsp:include page="load.jsp"></jsp:include>
        <jsp:include page="bottom.jsp"></jsp:include>

    </body>
</html>