<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/18
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Title</title>
        <link rel="stylesheet" href="/css/login.css">
        <link rel="stylesheet" href="//at.alicdn.com/t/font_2052253_aechmj40rfq.css"/>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <script src="/js/login.js"></script>
    </head>
    <body>
        <div id="logindiv" class="login-div">
            <div><span>帐号：</span>
                <input type="text" id="username" name="username">
                <span id="usernameTip">请输入正确的帐号</span>
            </div>
            <div><span>密码：</span>
                <input type="password" id="password" name="password">
                <span id="pwdTip">密码输入错误</span>
            </div>
            <div>
                <span>验证码:</span>
                <input type="text" id="code">
                <img src="/getVerifyImage" alt="" onclick="javascipt:$(this).prop('src','/getVerifyImage?'+window.Date.now())">
                <tip id="code_tip">验证码错误！</tip>
            </div>
            <div>
                <input type="checkbox" id="auto-login"><label for="auto-login">下次自动登录</label>
            </div>
            <div class="login" onclick="do_login()"><span>登录</span></div>
            <div id="verifyTip">帐号或密码错误，请重新输入</div>
            <a href="/jsp/register.jsp">用户注册</a>

        </div>

    </body>
</html>
