<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>产品展示-婚纱</title>
        <link rel="stylesheet" href="/css/commen.css"/>
        <link rel="stylesheet" href="/css/showPro.css"/>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <script src="/js/showPro.js"></script>

    </head>
    <body>
        <div class="nav">
            <jsp:include page="top.jsp"></jsp:include>
            <div class="show">
                <div class="title-text">产品展示</div>
                <div>
                    <div id="nav-show-pro">
                        <c:forEach items="${proType}" var="type">
                            <div onclick="getProductionByType(${type.id})">${type.name}</div>
                        </c:forEach>
                    </div>
                </div>
                <div id="pro_list">
                    <c:forEach items="${production}" var="pro">
                        <div>
                            <img src="${pro.proPhoto}" height="450" width="309"/>
                            <div>
                                <p>${pro.proName}</p>
                                <p><span>价格: </span><span>${pro.proPrice}</span></p>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </div>
            <jsp:include page="bottom.jsp"></jsp:include>
            <jsp:include page="load.jsp"></jsp:include>
        </div>
    </body>
</html>