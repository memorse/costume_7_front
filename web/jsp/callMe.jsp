<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>联系我们</title>
        <link rel="stylesheet" href="/css/commen.css">
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <link rel="stylesheet" href="/css/callMe.css">
        <script type="text/javascript" src="//api.map.baidu.com/api?type=webgl&v=1.0&ak=z8FGNf7HOrXb5a8ZcOEBLQrBZpvAW48c"></script>
    </head>

    <body>
        <jsp:include page="top.jsp"></jsp:include>
        <div class="title-text">
            <span>联系我们</span>
            <span>COTACT US</span>
        </div>
        <div id="main">
            <div>
                <div>
                    <div><img src="/images/4.jpg" height="700" width="550"/></div>
                    <div>
                        <div>
                            <div>联系我们</div>
                            <div>
                                <p>地址:${company.address}</p>
                                <p>电话:${company.phone}</p>
                                <p>QQ:${company.qq}</p>
                                <p>邮箱:${company.email}</p>
                            </div>
                        </div>
                        <div class="form">
                            <div>在线预约</div>
                            <div >
                                <div><span>姓名：</span><input type="text"></input></div>
                                <div><span>手机号码：</span><input type="text"></input></div>
                                <div><span>咨询内容：</span><textarea></textarea></div>
                                <div><input type="submit" value="提交"></input></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="allmap">
                    地图
                </div>
            </div>
        </div>
        <jsp:include page="load.jsp"></jsp:include>
        <jsp:include page="bottom.jsp"></jsp:include>

    </body>
    <script type="text/javascript">
        // GL版命名空间为BMapGL
        // 按住鼠标右键，修改倾斜角和角度
        var map = new BMapGL.Map("allmap");    // 创建Map实例
        map.centerAndZoom(new BMapGL.Point(116.404, 39.915), 11);  // 初始化地图,设置中心点坐标和地图级别
        map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        var local = new BMap.LocalSearch(map, {
            renderOptions:{map: map}
        });
        local.search("成都市孵化园9号楼E栋");
    </script>

</html>