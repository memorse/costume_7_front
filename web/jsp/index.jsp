<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/9/10
  Time: 10:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>服装定制</title>
        <link rel="stylesheet" href="/css/index.css"/>
        <link rel="stylesheet" href="//at.alicdn.com/t/font_2052253_aechmj40rfq.css"/>
        <script src="https://cdn.bootcdn.net/ajax/libs/jquery/1.10.0/jquery.js"></script>
        <style>
            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                display: flex;
                justify-content: center;
            }
        </style>
        <link rel="stylesheet" href="/css/commen.css"/>
    </head>
    <body>
        <div id="containner">
            <%--            /** */头部引用--%>
            <jsp:include page="top.jsp"></jsp:include>
            <%--    高端定制版块--%>
            <div id="high-ending">
                <%--                模块名称--%>
                <div id="high-text" class="title-text">
                    <span>高端定制服务</span>
                    <span>HI-END COSTOMIZATION</span>
                </div>
                <%--    展示图片--%>
                <div>
                    <img src="../images/1.jpg">
                    <img src="../images/2.jpg">
                    <img src="../images/3.jpg">
                </div>
            </div>
            <%--    GLAMORING版块--%>
            <div id="GLAMORING">
                <div>${company.name}</div>
                <div class="nav_g">
                    <div></div>
                    <div>
                        <div>${company.introduce}</div>
                        <div class="icon-btn">
                            <div>
                                <div></div>
                                <span>钻石品质</span>
                            </div>
                            <div>
                                <div></div>
                                <span>高端定制</span>
                            </div>
                            <div>
                                <div></div>
                                <span>极致服务</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <%--    产品展示版块--%>
            <div class="title-text">
                <span>作品展示</span>
                <span>APPRECIATION OF WORKS</span>
            </div>
            <div id="appreciation">

                <c:forEach items="${messages}" var="msg">
                    <div>
                        <div style="background: url('${msg.photo}');background-size: 100% 100%"></div>
                        <div>
                            <div>
                                <p>${msg.firstName}先生：${msg.husMessage}</p>
                                <p>${msg.firstName}太太：${msg.wifeMessage}</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
            <%--    新闻版块--%>
            <div id="news">
                <div>
                    <div class="title-text">
                        <span>新闻服务</span>
                        <span>NEWS INFORMATION</span>
                    </div>
                    <div>
                        <div class="news-menu">
                            <c:forEach var="news" items="${news}">
                                <div>
                                    <span class="iconfont icon-jiahao"></span>
                                    <span>${news.title}</span>
                                    <span><fmt:formatDate value="${news.pubTime}" type="date"/></span></div>
                            </c:forEach>
                        </div>
                        <div class="rool-photo">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
            <%--    底部引用--%>
            <jsp:include page="bottom.jsp"></jsp:include>

            <%--            登录版块--%>
            <jsp:include page="load.jsp"></jsp:include>
        </div>
    </body>
</html>
