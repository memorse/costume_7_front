$(function () {
    $("#glamorning").load("Glamorning.html")
    $.getScript("/js/glamorning.js",(res)=>{
        console.log("加载glamoring："+res)
    })

    getDataFromBack("/about","","get",(data)=>{
        console.log("about:"+data)
        loadDesignerToDiv(data.designers)
        loadOfflineStoreToDiv(data.offlineStores)
    })
})
var URL="http://localhost:7783";
function loadDesignerToDiv(data) {

    let htmlStr="";
    $.each(data,(i,designer)=>{
        if (i>3)
            return false;
        htmlStr+=`
        <div>
            <div><img src="${URL}${designer.photo}" height="500" width="750"/></div>
            <div>
                  <p>设计师：${designer.name}</p>
                  <p>${designer.introduce}</p>
            </div>
        </div>
        `
    })
    $("#designer").html(htmlStr);
}

function loadOfflineStoreToDiv(data) {
 $("#mianImage").attr("src",URL+data[0].photo);
    let htmlStr="";
    let store_address="";
    $.each(data,(i,offlinestore)=>{
        htmlStr+=`
        <div><img onclick="showBigImg('${offlinestore.photo}')"  src="${URL}${offlinestore.photo}" height="500" width="750"/></div>
        `
        store_address+=`
        <p>${offlinestore.address}</p>
        `
    })
    $("#scondImage").html(htmlStr);
    $("#store_address").html(store_address);
}

function showBigImg(url) {
    // console.log("change:"+url)
    $("#mianImage").prop("src",URL+url);
}