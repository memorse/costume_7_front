$(function () {
    getDataFromBack("/cusMade","","get",(data)=>{
        // console.log(data)
        loadStepToDiv(data.customMadeSteps,data.stepLoops)
    })
})

function loadStepToDiv(data,stepsloop) {
    let htmlStr="";
    for(var i =0;i<stepsloop;i++){
        htmlStr+="<div>"
        for(var j=i*3;j<=i*3+2;j++){
            if(data[j]==null ||data[j]=="")
                break;
        htmlStr+=`<div>
            <div>
            <div>
            <img src="${data[j].stepLogo}" height="100" width="100"/>
                <div>${data[j].stepName}</div>
                </div>
                </div>
                <div><span>${data[j].stepDetail}</span></div>
            </div>`
        }

        htmlStr+="</div>";
    }
        htmlStr+=`
        <div>
                    <div>
                        <p>注意事项</p>
                        <P>1.预约时请提供有效的联系方式和联系人姓名。</P>
                        <P>2.缴纳定金后还没制作坯衣时，若客户取
                            消订单，可以退回定金;一旦确认坯衣，
                            订单不能取消，定金不能退回。</P>
                        <P>3.因本产品是定制产品，一旦确认坯衣，不是质量问题，本店一律不退换。</P>
                    </div>
                </div>
        `
    $("#procedure").html(htmlStr);
}