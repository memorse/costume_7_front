var URL="http://localhost:7783";
$(function () {
    getDataFromBack("/news","","get",(data)=>{
        loadNewsToDiv(data.news);
        loadNewsTypeToDiv(data.newsTypes)
        loadHotNewsToDiv(data.news)
    })
})

function loadNewsToDiv(data) {
    let htmlStr=""
    $.each(data,(i,news)=>{
            htmlStr+=`
             <div>
                                    <span>${news.title}</span>
                                    <span>${news.pubTime.slice(0,10)}</span>
                                </div>
            `
    })
    $("#news-list").html(htmlStr);
}

function loadHotNewsToDiv(news) {


        let htmlStr=`
        <div><img src="${URL}${news[0].title}" height="134" width="197"/></div>
                        <div>
                            <p>${news[0].title}</p>
                            <p>${news[0].content}</p>
                            <p>${news[0].pubTime.slice(0,10)}</p>
                        </div>
        `;
    $("#hotnews").html(htmlStr);
}

function loadNewsTypeToDiv(data) {
    let htmlStr="";
    $.each(data,(i,newsType)=>{
        htmlStr+=`<a href="${newsType.priKey}"><div>${newsType.type}</div></a>`
    })
    $("#news_menu").html(htmlStr)
}