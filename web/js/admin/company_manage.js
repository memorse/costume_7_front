$(function () {

    getList();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getList();
        }
    })//绑定搜索框的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getList(1);
            }else if(page>=COUNT){
                getList(COUNT);
            }else {
                getList(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getList()
        }
    })//绑定分页显示条数

})
var LIST;
var CURRENT_PAGE;
var COUNT;



function getList(currentPage) {
    getListModel("/getCompanyByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        })
}
function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}
var URL="http://localhost:7783";
//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, company) {
        htmlStr += `<tr>
                <td><p>${company.id}</p></td>
                <td><p>${company.name}</p></td>
                <td><p>${company.enable == 1 ? "可用" : "不可用"}</p></td>
                <td><p>${company.phone}</p></td>
                <td><p>${company.email}</p></td>
                <td><p>${company.qq}</p></td>
                <td><p>${company.address}</p></td>
                <td><div class="table_img" style="background: url('${URL}${company.logo}');background-size: 100% 100%"></div></td>
                <td><div class="table_img" style="background: url('${URL}${company.qrCode}');background-size: 100% 100%"></div></td>
                <td><p>${company.ad}</p></td>
                <td><p>${company.introduce}</p></td>
                <td>
                    <div onclick="del(${company.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${company.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delCompany", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getList();
        } else {
            console.log("删除失败！");
        }

    })

}

//获取性别
function getGender() {
    //  let genders = $("input:radio[name=gender]");
    //
    //  for (let i=0;i<genders.length;i++){
    //     if(genders[i].checked){
    //         return genders[i].value;
    //     }
    // }
    return $('input:radio[name="gender"]:checked').val();
}

//获取爱好
function getHobby() {
    let hobyys = $("input:checkbox[name=hobby]");
    let hobby = "";
    for (let i = 0; i < hobyys.length; i++) {
        if (hobyys[i].checked) {
            hobby += hobyys[i].value + ","
        }
    }
    return hobby;
}

//获取输入信息，封装对象
function getCompanyObj() {
    let company = new Object();
    let id = $("#id").val();
    let com_name = $("#com_name").val();
    let com_phone = $("#com_phone").val();
    let com_email = $("#com_email").val();
    let com_advertisement = $("#com_advertisement").val();
    let com_qq = $("#com_qq").val();
    let com_address = $("#com_address").val();
    let com_intrduce = $("#com_introduce").val();

    company.id = id;
    company.name = com_name;
    company.email = com_email;
    company.phone = com_phone;
    company.qq = com_qq;
    company.ad = com_advertisement;
    company.address = com_address;
    company.introduce = com_intrduce;
    company.enable = getEnable();
    company.logo=$("#logo_value").val();
    company.qrcode=$("#qrcode_value").val();
    return company;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, company) => {
        console.log(i + "===>" + company)
        if (company.id == id) {

            data = company;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let company = searchUserFromLocal(id)
    $("#id").val(company.id)
    $("#com_name").val(company.name);
    $("#com_phone").val(company.phone);
    $("#com_address").val(company.address);
    $("#com_advertisement").val(company.ad);
    $("#com_email").val(company.email);
    $("#com_introduce").val(company.introduce);
    $("#com_qq").val(company.qq);
    $("#logo_value").val(company.logo);
    $("#qrcode_value").val(company.qrCode);
    setEnable(company.enable)
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#com_phone").val("");
    $("#com_name").val("");
    $("#com_email").val("");
    $("#com_qq").val("");
    $("#com_introduce").val("");
    $("#com_address").val("");
    $("#com_advertisement").val("");
    $("#logo_value").val("");
    $("#qrcode_value").val("");
    initEnable();
}

function verifyFormData() {
    let com_phone = $("#com_phone").val();
    let com_name = $("#com_name").val();
    let com_email = $("#com_email").val();

    let flag = true;
    if (!/[\S]{1,20}/.test(com_name)) {
        $("#com_name_tip").css("display", "block");
        flag = false;
    } else {
        $("#com_name_tip").css("display", "none");

    }
    if (!/^\d{11}$/.test(com_phone)) {
        $("#com_phone_tip").css("display", "block");
        flag = false;
    } else {
        $("#com_phone_tip").css("display", "none");

    }
    if (!/[\w]+@[\w]+\.[\w]+$/.test(com_email)) {
        $("#com_email_tip").css("display", "block");
        flag = false;
    } else {
        $("#com_email_tip").css("display", "none");
    }
    return flag;
}

//隐藏提示标签
function resetTipToNone() {
    $("#com_email_tip").css("display", "none");
    $("#com_name_tip").css("display", "none");
    $("#com_phone_tip").css("display", "none");

}
var URL="http://localhost:7783";
//添加
function add() {
    //1.验证输入合法性
    if (!verifyFormData())
        return;
    //2.获取输入信息
    let company = getCompanyObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addCompany",
        {company: JSON.stringify(company)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getList();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
        })


}

//修改
function modify() {
    if (!verifyFormData())
        return;
    $.post(URL+"/updateCompany",
        {company: JSON.stringify(getCompanyObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getList();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getList(1);
}

function lastPage() {
    getList(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getList(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getList(page >= COUNT ? COUNT : Number(page) + Number(1));
}