$(function () {

    getAllNewsSorts();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllNewsSorts
        }
    })//绑定搜索框的enter事件

    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getAllNewsSorts(1);
            }else if(page>=COUNT){
                getAllNewsSorts(COUNT);
            }else {
                getAllNewsSorts(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllNewsSorts()
        }
    })//绑定分页显示条数
})
var LIST;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";
function getAllNewsSorts(currentPage) {
    getListModel("/getNewsSortsByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        })
}

// function doSearch() {
//     let key = $("#key").val();
//     if (key != "") {
//         searchByKey(key);
//     } else {
//         getAllNewsSorts();
//     }
// }
//
// function searchByKey(key) {
//     $.get("/getNewsSortsByKey", {key: key}, function (data) {
//         data = JSON.parse(data)
//         if (data.status == 200) {
//             let users = data.newsSortList;
//             packaging(users);
//         } else {
//             console.log(data.status)
//         }
//     })
// }

//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, user) {
        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.type}</td>
                <td>${user.priKey}</td>
                <td>${user.enable==1?"可用":"不可用"}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delNewsSort", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getAllNewsSorts();
        } else {
            console.log("删除失败！");
        }

    })

}

//获取输入信息，封装对象
function getUserObj() {
    let newsSort=new Object();
    newsSort.id=$("#id").val()
    newsSort.type=$("#type").val()
    newsSort.prikey=$("#prikey").val()
    newsSort.enable=getEnable()
    return newsSort;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let user = searchUserFromLocal(id)
    $("#id").val(user.id)
    $("#type").val(user.type);
    $("#prikey").val(user.priKey);
    setEnable(user.enable)
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("");
    $("#type").val("");
    $("#prikey").val("");
    initEnable();
}

function verifyFormData() {
    let flag=true;
    if(!/[A-z]{1,20}/.test($("#prikey").val())){
        $("#prikey_tip").css("display","block");
        flag= false;
    }else {
        $("#prikey_tip").css("display","none");

    }
    if(!/[\S]{1,20}/.test($("#type").val())){
        $("#type_tip").css("display","block");
        flag= false;
    }else {
        $("#type_tip").css("display", "none");
    }
    return  flag;
}
//隐藏提示标签
function resetTipToNone() {
    $("#type_tip").css("display","none");
    $("#prikey_tip").css("display","none");
}
//添加
function add() {
    //1.验证输入合法性
    if(!verifyFormData())
        return;
    //2.获取输入信息
    let user = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addNewsSort",
        {newsSort: JSON.stringify(user)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getAllNewsSorts();
                $("#add_div").hide();
                remove_pointer();
                hidenUploadTip();
            } else {
                console.log("添加失败！");
            }
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateNewsSort",
        {newsSort: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getAllNewsSorts();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getAllNewsSorts(1);
}

function lastPage() {
    getAllNewsSorts(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getAllNewsSorts(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getAllNewsSorts(page >= COUNT ? COUNT : Number(page) + Number(1));
}
function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}