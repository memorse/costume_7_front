var URL="http://localhost:7783";
function uploadImg(id) {
    var resultFile = $("#uploadFile")[0].files[0];
    var formData=new FormData()
    formData.append('file',resultFile)
    $.ajax({
        url:URL+"/uploadImg",
        data:formData,
        dataType:"json",
        type:'post',
        processData:false,
        contentType:false,
        processData:false,
        success:(res)=>{
        console.log(res)
        if(res==""){
        $("#img_tip_0").css("display","inline-block");
        $("#img_tip_1").css("display","none");
    }else {
        $("#img_tip_1").css("display","inline-block");
        $("#img_tip_0").css("display","none");
        $("#"+id).val(res.url)
    }
},
    error:(err)=>{
        console.log(err)
    }
})
}
function uploadImg2(id,fileId,fileIndex) {
    var resultFile = $("#"+fileId)[0].files[0];
    var formData=new FormData()
    formData.append('file',resultFile)
    $.ajax({
        url:URL+"/uploadImg",
        data:formData,
        dataType:"json",
        type:'post',
        processData:false,
        contentType:false,
        processData:false,
        success:(res)=>{
        console.log(res)
        if(res==""){
            if(fileIndex==1){
                $("#img_tip_0_1").css("display","inline-block");
                $("#img_tip_1_1").css("display","none");
            }else if(fileIndex==2) {
                $("#img_tip_0_2").css("display","inline-block");
                $("#img_tip_1_2").css("display","none");
            }

    }else {
            if(fileIndex==1){
                $("#img_tip_1_1").css("display","inline-block");
                $("#img_tip_0_1").css("display","none");
            }else if(fileIndex==2) {
                $("#img_tip_1_2").css("display","inline-block");
                $("#img_tip_0_2").css("display","none");
            }
            $("#"+id).val(res.url)
    }
},
    error:(err)=>{
        console.log(err)
    }
})
}

function hidenUploadTip() {
    $("#img_tip_0").css("display","none");
    $("#img_tip_1").css("display","none");
    $("#uploadFile").val("")
}

function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}
function initEnable() {
    $("#enable_yes").prop("checked",true);
}
function setEnable(value) {
    if(value=="1"){
        $("#enable_yes").prop("checked",true);
    }else {
        $("#enable_no").prop("checked",true);
    }

}

function getListModel(url,currentPage,key,unitPage,packaging,getStaticData,getChildList) {
    let list;
    $.ajax({
        url:URL+url,
        dataType:"json",
        data:{
            currentPage:currentPage,
            key:key,
            unitPage:unitPage
        },
        success:function (data) {
            console.log(data);
            packaging(data.list);

            let datas =new Object();
            datas.currentPage=data.currentPage
            datas.count=data.count
            datas.list=data.list
            datas.unitPage=data.pageSize
            getStaticData(datas)
            getChildList(data.child_list)
        }
    })
}

function getListModel_1(url,currentPage,data,packaging,getStaticData,getChildList) {
    let list;
    $.ajax({
        url:URL+url,
        dataType:"json",
        data:{
            currentPage:currentPage,
            searchData:data
        },
        success:function (data) {
            packaging(data.list)
            let datas =new Object();
            datas.currentPage=data.currentPage
            datas.count=data.count
            datas.list=data.list
            datas.unitPage=data.pageSize
            getStaticData(datas)
            getChildList(data.child_list)
        }
    })
}

function getListModel_2(url,currentPage,key,unitPage,packaging,getStaticData,getChildList,getChildList2) {
    let list;
    $.ajax({
        url:URL+url,
        dataType:"json",
        data:{
            currentPage:currentPage,
            key:key,
            unitPage:unitPage
        },
        success:function (data) {
            packaging(data.list)
            let datas =new Object();
            datas.currentPage=data.currentPage
            datas.count=data.count
            datas.list=data.list
            datas.unitPage=data.pageSize
            getStaticData(datas)
            getChildList(data.child_list)
            getChildList2(data.child_list2)
        }
    })
}