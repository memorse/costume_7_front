$(function () {
    $("#content").load("/html/admin/user_manage.html");
    let logindata=JSON.parse(sessionStorage.getItem("logindata"));
    $("#user_name").text(logindata.username);
})
function showContent(url) {
    $("#content").load(url);
}

function quit() {
    sessionStorage.removeItem("logindata")
    window.location.href="../../html/admin/admin_login.html";
}