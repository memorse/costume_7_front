$(function () {

    getAllProduction();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllProduction();
        }
    })//绑定搜索框的enter事件

    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getAllProduction(1);
            }else if(page>=COUNT){
                getAllProduction(COUNT);
            }else {
                getAllProduction(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllProduction()
        }
    })//绑定分页显示条数
})
var ALL_PRO;
var DESIGNER;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";
function getAllProduction(currentPage) {
    getListModel_2("/getProductionsByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        },
        (child)=>{
        DESIGNER=child
        loadLevelsToAdd(child)
        },
        (child2)=>{
            loadTypesToAdd(child2)
        })
}

function doSearch() {
    getAllProduction();
}
//
// function searchByKey(key) {
//     $.get("/getProductionsByKey", {key: key}, function (data) {
//         data = JSON.parse(data)
//         if (data.status == 200) {
//             let users = data.productions;
//             packaging(users);
//         } else {
//             console.log(data.status)
//         }
//     })
// }

//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, user) {
        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.proName}</td>
                <td>${user.proPrice}</td>
                <td><div class="table_img" style="background: url('${URL}${user.proPhoto}');background-size: 100% 100%"></div></td>
                <td>${user.type.name}</td>
                <td>${user.designer.name}</td>
                <td>${user.enable==1?"是":"否"}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delProduction", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getAllProduction();
        } else {
            console.log("删除失败！");
        }

    })

}

//获取输入信息，封装对象
function getUserObj() {
    let production = new Object();
    production.id = $("#id").val();
    production.name = $("#name").val();
    production.price = $("#price").val();
    production.photo = $("#photo").val();
    production.type = $("#type").val();
    production.designer = $("#designer").val();
    production.enable=getEnable();
    return production;

}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(ALL_PRO, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}
function loadLevelsToAdd(obj) {
    let htmlStr="";
    $.each(obj,(i,v)=>{

        htmlStr+=`
        <option value="${v.id}">${v.name}</option>
        `

    })
    $("#designer").html(htmlStr)
}
function loadTypesToAdd(obj) {
    let htmlStr="";
    $.each(obj,(i,v)=>{

        htmlStr+=`
        <option value="${v.id}">${v.name}</option>
        `

    })
    $("#type").html(htmlStr)
}
//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let user = searchUserFromLocal(id)
    $("#id").val(user.id)
    $("#name").val(user.proName);
    $("#price").val(user.proPrice);
    $("#photo").val(user.proPhoto);
    $("#designer option[value="+user.designer.id+"]").prop("selected",true);
    $("#type option[value="+user.type.id+"]").prop("selected",true);
    setEnable(user.enable);
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#name").val("");
    $("#price").val("");
    $("#photo").val("");
    $("#designer").val("");
    $("#type").val("");
    initEnable();
}

function verifyFormData() {
    let flag=true;
    if (!/[\S]{1,20}/.test($("#name").val())) {
        $("#name_tip").css("display", "block");
        flag = false;
    } else {
        $("#name_tip").css("display", "none");

    }
    if (!/[\S]{1,20}/.test($("#designer").val())) {
        $("#designer_tip").css("display", "block");
        flag = false;
    } else {
        $("#designer_tip").css("display", "none");

    }
    if (!/\d+\.?\d?/.test($("#price").val())) {
        $("#price_tip").css("display", "block");
        flag = false;
    } else {
        $("#price_tip").css("display", "none");

    }
    return flag;
}

//隐藏提示标签
function resetTipToNone() {

    $("#name_tip").css("display", "none");
    $("#prikey_tip").css("display", "none");
    $("#designer_tip").css("display", "none");


}

//添加
function add() {
    //1.验证输入合法性
    if (!verifyFormData())
        return;
    //2.获取输入信息
    let user = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addProduction",
        {production: JSON.stringify(user)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getAllProduction();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
            hidenUploadTip()
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateProduction",
        {production: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getAllProduction();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
            hidenUploadTip()
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}
//分页模块
function firstPage() {
    getAllProduction(1);
}

function lastPage() {
    getAllProduction(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getAllProduction(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getAllProduction(page >= COUNT ? COUNT : Number(page) + Number(1));
}
function loadStaticData(page, count, list) {
    ALL_PRO = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}