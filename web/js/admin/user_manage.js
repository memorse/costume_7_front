$(function () {

    getAllUsers();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllUsers();
        }
    })//绑定搜索框的enter事件

    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getAllUsers(1);
            }else if(page>=COUNT){
                getAllUsers(COUNT);
            }else {
                getAllUsers(page)
            }

        }
    })//绑定分页查询的enter事件
    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
                getAllUsers()
        }
    })//绑定分页显示条数
})
var All_USERS;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";

function getAllUsers(currentPage) {
    // getListModel("/getUsersByKey",
    //     currentPage,
    //     $("#key").val(),
    //     $("#unitPage").val(),
    //     (list) => {
    //         packaging(list)
    //     },
    //     (datas) => {
    //         $("#unitPage").val(datas.unitPage);
    //         loadStaticData(datas.currentPage, datas.count, datas.list);
    //     },
    //     (child)=>{
    //
    //     })
    getListModel_1("/getUsersByKey",
        currentPage,
        JSON.stringify(getSearchData()),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        },
        (child)=>{

        })
}
function loadStaticData(page, count, list) {
    All_USERS = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}

function doSearch() {
    getAllUsers();
}

// function searchByKey(key) {
//     $.ajax({
//         type:"get",
//         url:"/getUsersByKey",
//         dataType:"json",
//         data:{key: key},
//         success:function (data) {
//             // data = JSON.parse(data)
//             if (data.status == 200) {
//                 let users = data.users;
//                 packaging(users);
//             } else {
//                 console.log(data.status)
//             }
//         }
//     });
// }
//加密
function jiami(str) {
    let result="";
    for(i=0;i<str.length;i++){
        result+=str[i].replace(str[i],"*")
    }
    return result;
}

//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, user) {
        let hobbs = ""
        let hobby=user.hobby.split(",");
        $.each(hobby, function (i, val) {
            if (i == user.hobby.length - 1) {
                hobbs += val;
            } else {
                hobbs += val + ",";
            }

        })
        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.account}</td>
                <td>${ jiami('password') }</td>
                <td>${user.name}</td>
                <td>${user.nickName}</td>
                <td>${user.isAdmin==1?"是":"否"}</td>
                <td>${user.enable==1?"可用":"不可用"}</td>
                <td>${user.phone}</td>
                <td>${user.gender}</td>
                <td>${user.mate}</td>
                <td>${hobbs}</td>
                <td>${user.birthday}</td>
                <td>${user.weddingAnniversary}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delUser", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getAllUsers();
        } else {
            console.log("删除失败！");
        }

    })

}


function getIsAdmin() {
    return $('input:radio[name="is_admin"]:checked').val();
}
//获取性别
function getGender() {
    return $('input:radio[name="gender"]:checked').val();
}
//获取爱好
function getHobby() {
    let hobyys = $("input:checkbox[name=hobby]");
    let hobby = "";
    for (let i = 0; i < hobyys.length; i++) {
        if (hobyys[i].checked) {
            hobby += hobyys[i].value + ","
        }
    }
    return hobby;
}
//获取搜索性别条件
function getSearchGender() {
    return $('input:radio[name="search_gender"]:checked').val();
}
//获取搜索爱好条件
function getSearchHobby() {
    let hobyys = $("input:checkbox[name=search_hobby]");
    let hobby = "";
    for (let i = 0; i < hobyys.length; i++) {
        if (hobyys[i].checked) {
            hobby += hobyys[i].value + ","
        }
    }
    return hobby;

}

//获取搜索条件
function getSearchData(){
    let search=new Object()
    search.gender=getSearchGender();
    search.hobby=getSearchHobby();
    search.begin_date=$("#search_date_begin").val()
    search.end_date=$("#search_date_end").val()
    search.key=$("#key").val()
    search.currentPage=$("#current_page").val()
    search.pageSize=$("#unitPage").val()
    return search
}

//获取输入信息，封装对象
function getUserObj() {
    let user = new Object();
    let id = $("#id").val();
    let user_account = $("#user_account").val();
    let user_name = $("#user_name").val();
    let user_nickname = $("#user_nickname").val();
    let user_birthday = $("#user_birthday").val();
    let user_mate = $("#user_mate").val();
    let user_wedding_anniversary = $("#user_wedding_anniversary").val();
    let user_phone = $("#user_phone").val();
    let user_password = $("#user_password").val();
    user.id = id;
    user.user_account = user_account;
    user.user_password = user_password;
    user.user_name = user_name;
    user.user_nickname = user_nickname;
    user.user_phone = user_phone;
    user.user_gender = getGender();
    user.user_mate = user_mate;
    user.user_hobby = getHobby();
    user.is_admin=getIsAdmin();
    user.enable=getEnable();
    user.user_birthday = user_birthday;
    user.user_wedding_anniversary = user_wedding_anniversary;
    return user;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(All_USERS, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let user = searchUserFromLocal(id)
    $("#id").val(user.id)
    $("#user_account").val(user.account);
    $("#user_name").val(user.name);
    $("#user_nickname").val(user.nickName);
    $("#user_birthday").val(user.birthday);
    $("#user_mate").val(user.mate);
    $("#user_wedding_anniversary").val(user.weddingAnniversary);
    $("#user_phone").val(user.phone);
    $("#user_password").val(user.password);
    if (user.gender == "男") {
        $("#user_gender_male").prop("checked", 'checked');
    } else {
        $("#user_gender_fmale").prop("checked", 'checked');
    }
    if (user.isAdmin == "1") {
        $("#is_admin_yes").prop("checked", 'checked');
    } else {
        $("#is_admin_no").prop("checked", 'checked');
    }
    $.each(user.hobby, (i, val) => {

        if (val == "打球") {
            $("#user_hobby_playtball").prop("checked", true);
        }
        else if (val == "看书") {
            $("#user_hobby_lookbook").prop("checked", true);
        }
        else if (val == "音乐") {
            $("#user_hobby_music").prop("checked", true);
        }
        else if (val == "其它"){
            $("#user_hobby_other").prop("checked", true);
        }
    })
    setEnable(user.enable)

}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#user_account").val("");
    $("#user_name").val("");
    $("#user_nickname").val("");
    $("#user_birthday").val("");
    $("#user_mate").val("");
    $("#user_wedding_anniversary").val("");
    $("#user_phone").val("");
    $("#user_password").val("");
    $("#confirm_password").val("");
    $("#user_gender_male").prop("checked", 'checked');
    $("#user_hobby_playtball").prop("checked", false);
    $("#user_hobby_lookbook").prop("checked", false);
    $("#user_hobby_music").prop("checked", false);
    $("#user_hobby_other").prop("checked", false);
    $("#is_admin_no").prop("checked", 'checked');
    initEnable()
}

function verifyFormData() {
    let user_account = $("#user_account").val();
    let user_name = $("#user_name").val();
    let user_nickname = $("#user_nickname").val();
    let user_password = $("#user_password").val();
    let confirm_password = $("#confirm_password").val();
    let flag=true;
    if(!/^[A-z]\w{5,16}$/.test(user_account)){
        $("#account_tip").css("display","block");
        flag= false;
    }else {
        $("#account_tip").css("display","none");

    }
    if(!/[\S]{1,20}/.test(user_name)){
        $("#name_tip").css("display","block");
        flag= false;
    }else {
        $("#name_tip").css("display","none");

    }
    if(!/[\S]{1,20}/.test(user_nickname)){
        $("#nickname_tip").css("display","block");
        flag= false;
    }else {
        $("#nickname_tip").css("display","none");

    }
    if(!/\w{6,16}$/.test(user_password)){
        $("#password_tip").css("display","block");
        flag= false;
    }else {
        $("#password_tip").css("display","none");

    }
    if(confirm_password!=user_password){
        $("#confirm_password_tip").css("display","block");
        flag= false;
    }else {
        $("#confirm_password_tip").css("display","none");

    }
    return  flag;
}
//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}
//添加
function add() {
    //1.验证输入合法性
    if(!verifyFormData())
        return;
    //2.获取输入信息
    let user = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addUser",
        {user: JSON.stringify(user)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getAllUsers();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateUser",
        {user: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getAllUsers();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getAllUsers(1);
}

function lastPage() {
    getAllUsers(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getAllUsers(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getAllUsers(page >= COUNT ? COUNT : Number(page) + Number(1));
}
