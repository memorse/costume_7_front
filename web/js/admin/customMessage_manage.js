$(function () {

    getMessages();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getMessages();
        }
    })//绑定搜索框的enter事件

    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getMessages(1);
            }else if(page>=COUNT){
                getMessages(COUNT);
            }else {
                getMessages(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getMessages()
        }
    })//绑定分页显示条数
})
var LIST;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";
function getMessages(currentPage) {
    getListModel("/getMsgByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        })
}
//
function doSearch() {
    getMessages();
}
//
// function searchByKey(key) {
//     $.get("/getMsgByKey", {key: key}, function (data) {
//         data = JSON.parse(data)
//         if (data.status == 200) {
//             let users = data.messages;
//             console.log(data.messages)
//             console.log("------")
//             console.log(JSON.parse(users))
//             packaging(JSON.parse(users));
//         } else {
//             console.log(data.status)
//         }
//     })
// }

//封装html代码并加载进网页
function packaging(obj) {
    console.log(obj)
    console.log("---------")

    let htmlStr = "";
    $.each(obj, function (val, user) {
        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td><p>${user.husMessage}</p></td>
                <td><p>${user.wifeMessage}</p></td>
                <td><div class="table_img" style="background: url('${URL}${user.photo}');background-size: 100% 100%"></div></td>
                <td>${user.enable==1?"是":"否"}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delMsg", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getMessages();
        } else {
            console.log("删除失败！");
        }

    })
}

//获取输入信息，封装对象
function getUserObj() {
    let user = new Object();
    user.id = $("#id").val();
    user.firstName = $("#firstName").val();
    user.husMsg = $("#husband").val();
    user.wifeMsg = $("#wife").val();
    user.photo=$("#photo_value").val()
    user.enable=getEnable();
    return user;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let user = searchUserFromLocal(id)
    $("#id").val(user.id);
    $("#firstName").val(user.firstName);
    $("#husband").val(user.husMessage);
    $("#wife").val(user.wifeMessage);
    $("#photo_value").val(user.photo);
    setEnable(user.enable);

}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("");
    $("#firstName").val("");
    $("#husband").val("");
    $("#wife").val("");
    $("#photo_value").val("");
    initEnable();

}

function verifyFormData() {
    let flag=true;
    if(!/[\S]{1,20}/.test($("#firstName").val())){
        $("#firstName_tip").css("display","block");
        flag= false;
    }else {
        $("#firstName_tip").css("display","none");

    }
    if(!/[\S]+/.test($("#husband").val())){
        $("#husband_tip").css("display","block");
        flag= false;
    }else {
        $("#husband_tip").css("display","none");

    }
    if(!/[\S]+/.test($("#wife").val())){
        $("#wife_tip").css("display","block");
        flag= false;
    }else {
        $("#wife_tip").css("display","none");

    }
    return  flag;
}
//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}
//添加
function add() {
    //1.验证输入合法性
    if(!verifyFormData())
        return;
    //2.获取输入信息
    let user = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addMsg",
        {message: JSON.stringify(user)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getMessages();
                $("#add_div").hide();
                remove_pointer();

            } else {
                console.log("添加失败！");
            }
            hidenUploadTip();
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateMsg",
        {message: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getMessages();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
            hidenUploadTip()
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getMessages(1);
}

function lastPage() {
    getMessages(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getMessages(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getMessages(page >= COUNT ? COUNT : Number(page) + Number(1));
}

function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}