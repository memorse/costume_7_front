$(function () {

    getList();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getList();
        }
    })//绑定搜索框的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getList(1);
            }else if(page>=COUNT){
                getList(COUNT);
            }else {
                getList(page)
            }

        }
    })//Fenye

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getList()
        }
    })//绑定分页显示条数

})
var LIST;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";
    function getList(currentPage) {
        getListModel("/getCmadeByKey",
            currentPage,
            $("#key").val(),
            $("#unitPage").val(),
            (list) => {
                packaging(list)
            },
            (datas) => {
                $("#unitPage").val(datas.unitPage);
                loadStaticData(datas.currentPage, datas.count, datas.list);
            })
    }
function doSearch() {
    let key = $("#key").val();
    if (key != "") {
        searchByKey(key);
    } else {
        getList();
    }
}

function searchByKey(key) {
    $.get(URL+"/getCmadeByKey", {key: key}, function (data) {
        data = JSON.parse(data)
        if (data.status == 200) {

            packaging(data.list);
        } else {
            console.log(data.status)
        }
    })
}

//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, user) {
        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.stepName}</td>
                <td><div class="table_img" style="background: url('${URL}${user.stepLogo}');background-size: 100%"></div></td>
                <td>${user.stepOrder}</td>
                <td>${user.stepDetail}</td>
                <td>${user.enable==1?"可用":"不可用"}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let cmade = searchUserFromLocal(id)
    $("#id").val(cmade.id);
    $("#step_detail").val(cmade.stepDetail);
    $("#step_order").val(cmade.stepOrder);
    $("#step_name").val(cmade.stepName);
    $("#img_value").val(cmade.stepLogo);
    setEnable(cmade.enable);
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#step_detail").val("");
    $("#step_order").val("");
    $("#uploadFile").val("");
    $("#img_value").val("");
    $("#step_name").val("");
    initEnable();

}

//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}


//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delCmade", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getList();
        } else {
            console.log("删除失败！");
        }

    })

}

//添加
function add() {
    //1.验证输入合法性
    if(!verifyFormData())
        return;
    //2.获取输入信息
    let cmade = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addCmade",
        {cmade: JSON.stringify(cmade)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getList();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
            hidenUploadTip()
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateCmade",
        {cmade: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getList();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
            hidenUploadTip()
        })
}

//获取输入信息，封装对象
function getUserObj() {
    let cmade = new Object();
    cmade.id = $("#id").val();
    cmade.name = $("#step_name").val();
    cmade.order = $("#step_order").val();
    cmade.logo = $("#img_value").val();
    cmade.detail = $("#step_detail").val();
    cmade.enable=getEnable();
    return cmade;
}

//验证输入合法性
function verifyFormData() {
    let flag=true;
    if(!/[\S]{1,20}/.test($("#step_name").val())){
        $("#step_name_tip").css("display","block");
        flag= false;
    }else {
        $("#step_name_tip").css("display","none");

    }
    if(!/\d+/.test($("#step_order").val())){
        $("#step_order_tip").css("display","block");
        flag= false;
    }else {
        $("#step_order_tip").css("display","none");

    }
    return  flag;
}
//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getList(1);
}

function lastPage() {
    getList(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getList(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getList(page >= COUNT ? COUNT : Number(page) + Number(1));
}

function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}