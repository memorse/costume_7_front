$(function () {

    getAllNews();

    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllNews();
        }
    })//绑定搜索框的enter事件

    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getAllNews(1);
            }else if(page>=COUNT){
                getAllNews(COUNT);
            }else {
                getAllNews(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getAllNews()
        }
    })//绑定分页显示条数
})
var LIST;
var CURRENT_PAGE;
var COUNT;
var URL="http://localhost:7783";
function getAllNews(currentPage) {
    getListModel("/getNewsByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list)
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        },
        (child)=>{

            loadLevelsToAdd(child)
        })
}

function loadLevelsToAdd(obj) {
    let htmlStr="";
    $.each(obj,(i,v)=>{

        htmlStr+=`
        <option value="${v.id}">${v.type}</option>
        `

    })
    $("#sort").html(htmlStr)
}
function doSearch() {
    getAllNews();
}
//
// function searchByKey(key) {
//     $.get("/getNewsByKey", {key: key}, function (data) {
//         data = JSON.parse(data)
//         if (data.status == 200) {
//             let news = data.newsList;
//             loadSortsToAdd(data.newsSort)
//             packaging(news);
//         } else {
//             console.log(data.status)
//         }
//     })
// }

//封装html代码并加载进网页
function packaging(obj) {
    let htmlStr = "";
    $.each(obj, function (val, news) {
        htmlStr += `<tr>
                <td>${news.id}</td>
                <td><p>${news.title}</p></td>
                <td>${news.newsSort.type}</td>
                <td><div class="table_img" style="background: url('${URL}${news.imgRoute}');background-size: 100% 100%"></div></td>
                <td>${news.enable==1?"可用":"不可用"}</td>
                <td>${news.content}</td>
                <td>${news.pubTime}</td>
                <td>
                    <div onclick="del(${news.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${news.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}
//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delNews", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getAllNews();
        } else {
            console.log("删除失败！");
        }


    })

}

//获取输入信息，封装对象
function getUserObj() {
    let news = new Object();
    news.id = $("#id").val()
    news.title = $("#title").val()
    news.imgRoute = $("#img_value").val()
    news.content = $("#newsContent").val()
    news.pubTime = $("#pubTime").val()
    news.enable=getEnable();
    news.news_sort=$("#sort").val()
    return news;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, news) => {
        console.log(i + "===>" + news)
        if (news.id == id) {
            data = news;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let news = searchUserFromLocal(id)
    $("#id").val(news.id)
    $("#title").val(news.title)
    $("#img_value").val(news.imgRoute)
    $("#newsContent").val(news.content)
    $("#pubTime").val(news.pubTime)
    $("#sort option[value="+news.newsSort.id+"]").prop("selected",true)
    setEnable(news.enable)
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#title").val("")
    $("#img_value").val("")
    $("#newsContent").val("")
    $("#pubTime").val("")
    $("#sort").val("")
    initEnable();
}

function verifyFormData() {
    let flag=true;
    if (!/[\S]+/.test($("#title").val())) {
        $("#title_tip").css("display", "block");
        flag = false;
    } else {
        $("#title_tip").css("display", "none");
    }
    if (!/[\S]+/.test($("#newsContent").val())) {
        $("#newsContent_tip").css("display", "block");
        flag = false;
    } else {
        $("#newsContent_tip").css("display", "none");
    }
    if($("#pubTime").val()==""){
        $("#pubTime_tip").css("display", "block");
        flag = false;
    }else {
        $("#pubTime_tip").css("display", "none");
    }
    return flag;
}

//隐藏提示标签
function resetTipToNone() {
    $("#newsContent_tip").css("display", "none");
    $("#title_tip").css("display", "none");
}

//添加
function add() {
    //1.验证输入合法性
    if (!verifyFormData())
        return;
    //2.获取输入信息
    let news = getUserObj();
    // console.log(JSON.stringify(news))
    //3.提交
    $.post(
        URL+"/addNews",
        {news: JSON.stringify(news)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getAllNews();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
            hidenUploadTip();
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateNews",
        {news: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getAllNews();
                $("#add_div").hide();
                remove_pointer();
            } else {
                console.log("修改失败！");
            }
            hidenUploadTip();
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getAllNews(1);
}

function lastPage() {
    getAllNews(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getAllNews(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getAllNews(page >= COUNT ? COUNT : Number(page) + Number(1));
}
function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}