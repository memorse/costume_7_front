   function quit() {
        // var login=document.getElementById("logindiv");

        $("#logindiv").hide();
    }
    function do_login() {
        if(verifyUsername()){
            $("#usernameTip").show();
            return;
        }else{
            $("#usernameTip").hide();
        }
        if(verifyPassword()){
            $("#pwdTip").show()
            return;
        }else {
            $("#pwdTip").hide()
        }
        $.ajax({
            type:"post",
            url:"/doLogin",
            dateType:"json",
            data:{
                username:$("#username").val(),
                password:$("#password").val()

            },
            success:function (data) {
                // console.log(data)
                data=JSON.parse(data)
                if(data.status==200){
                    sessionStorage.setItem("logindata",JSON.stringify(data))
                    quit();
                    window.open("/html/admin/manage.html");
                }else{
                    $("#verifyTip").show();

                }

            },
            error:function (err) {
                console.log(err)
            }
        })

    }

    function verifyUsername() {
       return  $("#username").val()==""
    } function verifyPassword() {
       return  $("#password").val()==""
   }
   $(document).bind("keydown",function (e) {
       if(e.keyCode=="13"){
           do_login();
       }
   })