$(function () {

    getDesigners();
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getDesigners();
        }
    })//绑定搜索框的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            if(page<=1){
                getDesigners(1);
            }else if(page>=COUNT){
                getDesigners(COUNT);
            }else {
                getDesigners(page)
            }

        }
    })//绑定分页查询的enter事件

    $("#unitPage").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getDesigners()
        }
    })//绑定分页显示条数
})
var LIST;
var CURRENT_PAGE;
var COUNT;
var LEVEL_LIST;
var URL="http://localhost:7783";
function getDesigners(currentPage) {
    getListModel("/getDisignersByKey",
        currentPage,
        $("#key").val(),
        $("#unitPage").val(),
        (list) => {
            packaging(list);
        },
        (datas) => {
            $("#unitPage").val(datas.unitPage);
            loadStaticData(datas.currentPage, datas.count, datas.list);
        },
        (child)=>{
            LEVEL_LIST=child;
            loadLevelsToAdd(child)
        })
}
function loadLevelsToAdd(obj) {
    let htmlStr="";
    $.each(obj,(i,v)=>{

        htmlStr+=`
        <option value="${v.id}">${v.levelName}</option>
        `

    })
    $("#level").html(htmlStr)
}
function doSearch() {
    getDesigners();
}
//
// function searchByKey(key) {
//     $.get("/getDisignersByKey", {key: key}, function (data) {
//         data = JSON.parse(data)
//         if (data.status == 200) {
//             let users = data.designers;
//             packaging(users);
//         } else {
//             console.log(data.status)
//         }
//     })
// }

//封装html代码并加载进网页
function packaging(obj) {

    let htmlStr = "";
    $.each(obj, function (val, user) {

        htmlStr += `<tr>
                <td>${user.id}</td>
                <td>${user.name}</td>
                <td>${user.gender}</td>
                <td>${user.level.levelName}</td>
                <td><div class="table_img" style="background: url('${URL}${user.photo}');background-size: 100% 100%"></div></td>
                <td>${user.introduce}</td>
                <td>${user.enable==1?"是":"否"}</td>
                <td>
                    <div onclick="del(${user.id})">删除</div>
                    <div onclick="switchBtn(2);setUserToModify(${user.id})">修改</div>
                </td>
            </tr>`;
    });
    $("tbody").html(htmlStr);
}

//s删除
function del(id) {
    if (!confirm("确认删除嘛？"))
        return;
    $.post(URL+"/delDesigner", {id: id}, function (res) {
        res = JSON.parse(res);
        if (res > 0) {
            console.log("删除成功！");
            getDesigners();
        } else {
            console.log("删除失败！");
        }

    })

}

//获取性别
function getGender() {

    return $('input:radio[name="gender"]:checked').val();
}

//获取输入信息，封装对象
function getUserObj() {
    let user = new Object();
    user.id = $("#id").val()
    user.name = $("#name").val()
    user.gender = getGender()
    user.photo = $("#photo_value").val()
    user.introduce = $("#introduction").val()
    user.level = $("#level").val()
    user.enable=getEnable();
    return user;
}


//从js对象中取查找数据
function searchUserFromLocal(id) {
    let data = null;
    $.each(LIST, (i, user) => {
        console.log(i + "===>" + user)
        if (user.id == id) {

            data = user;
            return false;
        }
    })
    return data;
}

//加载对象信息到编辑框
function setUserToModify(id) {
    resetTipToNone();
    resetUserToModify();
    let user = searchUserFromLocal(id)
    $("#id").val(user.id)
    $("#name").val(user.name)
    $("#photo_value").val(user.photo)
    $("#introduction").val(user.introduce)
    if (user.gender == "男") {
        $("#user_gender_male").prop("checked", 'checked');
    } else {
        $("#user_gender_fmale").prop("checked", 'checked');
    }
    $("#level option[value="+user.level.id+"]").prop("selected",true);
    setEnable(user.enable)
}

// 清楚编辑框参数
function resetUserToModify() {
    $("#id").val("")
    $("#name").val("")
    $("#photo_value").val("")
    $("#introduction").val("")
    $("#level").val("")
    $("#user_gender_male").prop("checked", 'checked');
    initEnable();
}

function verifyFormData() {
    let flag=true;

    if (!/[\S]{1,20}/.test($("#name").val())) {
        $("#name_tip").css("display", "block");
        flag = false;
    } else {
        $("#name_tip").css("display", "none");
    }

    return flag;
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display", "none");
    $("#password_tip").css("display", "none");
    $("#nickname_tip").css("display", "none");
    $("#name_tip").css("display", "none");
    $("#account_tip").css("display", "none");
}

//添加
function add() {
    //1.验证输入合法性
    if (!verifyFormData())
        return;
    //2.获取输入信息
    let user = getUserObj();
    // console.log(JSON.stringify(user))
    //3.提交
    $.post(
        URL+"/addDesigner",
        {designer: JSON.stringify(user)},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("添加成功！");
                getDesigners();
                $("#add_div").hide();
                hidenUploadTip();
                remove_pointer();
            } else {
                console.log("添加失败！");
            }
        })


}

//修改
function modify() {
    // let id=$("#id").val();
    // setUserToModify(searchUserFromLocal(id))
    $.post(URL+"/updateDesigner",
        {designer: JSON.stringify(getUserObj())},
        function (res) {
            res = JSON.parse(res);
            if (res > 0) {
                console.log("修改成功！");
                getDesigners();
                $("#add_div").hide();
                hidenUploadTip();
                remove_pointer();
            } else {
                console.log("修改失败！");
                alert("修改失败！");
                hidenUploadTip();
            }
        })
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetUserToModify();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();
    }
}

//分页模块
function firstPage() {
    getDesigners(1);
}

function lastPage() {
    getDesigners(COUNT);
}

function prePage() {
    let page = $("#current_page").val();
    getDesigners(page <= 1 ? 1 : Number(page) - Number(1));
}

function nextPage() {
    let page = $("#current_page").val();
    getDesigners(page >= COUNT ? COUNT : Number(page) + Number(1));
}
function loadStaticData(page, count, list) {
    LIST = list;
    CURRENT_PAGE = page;
    COUNT = count;
    $("#current_page").val(page)
    $("#all_page").text(count)
}