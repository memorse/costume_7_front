var URL="http://localhost:7783";
$(function () {
    getDataFromBack("/production","","get",(data)=>{
        loadProductionToDiv(data.productions);
        loadProductionTypeToDiv(data.productionTypes);
    })
})
function loadProductionToDiv(data) {
    let htmlStr="";
    $.each(data,(i,v)=>{
        htmlStr+=`
                    <div>
                            <img src="${v.proPhoto}" height="450" width="309"/>
                            <div>
                                <p>${v.proName}</p>
                                <p><span>价格: </span><span>${v.proPrice}</span></p>
                            </div>
                        </div>`
    })
    $("#pro_list").html(htmlStr)
}

function loadProductionTypeToDiv(data) {
    let htmlStr="";
    $.each(data,(i,type)=>{
        htmlStr+=`
                    <div onclick="getProductionByType(${type.id})">${type.name}</div>
                `
    });
    $("#nav-show-pro").html(htmlStr)
}

function getProductionByType(pro_type){
    $.get(
        URL+"/getProductionByType",
        {pro_type:pro_type},
        (res)=>{
            // console.log(res)
            if(res.productions.length==0){
                $("#pro_list").html(`
                <div style="color: red;font-size: 16px">抱歉！暂无相关产品>_<</div>
                `)
            }else {
                let htmlStr="";
                $.each(res.productions,(i,v)=>{
                    htmlStr+=`
                    <div>
                            <img src="${v.proPhoto}" height="450" width="309"/>
                            <div>
                                <p>${v.proName}</p>
                                <p><span>价格: </span><span>${v.proPrice}</span></p>
                            </div>
                        </div>`
                });
                $("#pro_list").html(htmlStr);
            }
        })
}