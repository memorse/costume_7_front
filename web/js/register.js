function quit() {
    $("#register_div").hide();
    $("#logindiv").hide();
}

//获取性别
function getGender() {
    return $('input:radio[name="gender"]:checked').val();
}
function getIsAdmin() {
    return $('input:radio[name="is_admin"]:checked').val();
}

//获取爱好
function getHobby() {
    let hobyys = $("input:checkbox[name=hobby]");
    let hobby = "";
    for (let i = 0; i < hobyys.length; i++) {
        if (hobyys[i].checked) {
            hobby += hobyys[i].value + ","
        }
    }
    return hobby;
}

//获取输入信息，封装对象
function getUserObj() {
    let user = new Object();
    let id = $("#id").val();
    let user_account = $("#user_account").val();
    let user_name = $("#user_name").val();
    let user_nickname = $("#user_nickname").val();
    let user_birthday = $("#user_birthday").val();
    let user_mate = $("#user_mate").val();
    let user_wedding_anniversary = $("#user_wedding_anniversary").val();
    let user_phone = $("#user_phone").val();
    let user_password = $("#user_password").val();
    user.id = "";
    user.user_account = user_account;
    user.user_password = user_password;
    user.user_name = user_name;
    user.user_nickname = user_nickname;
    user.user_phone = user_phone;
    user.user_gender = getGender();
    user.user_mate = user_mate;
    user.user_hobby = getHobby();
    user.is_admin=getIsAdmin();
    user.enable=$('input:radio[name="enable"]:checked').val();
    user.user_birthday = user_birthday;
    user.user_wedding_anniversary = user_wedding_anniversary;
    return user;
}

function verifyFormData() {
    let user_account = $("#user_account").val();
    let user_name = $("#user_name").val();
    let user_nickname = $("#user_nickname").val();
    let user_password = $("#user_password").val();
    let confirm_password = $("#confirm_password").val();
    let flag=true;
    if(!/^[A-z]\w{5,16}$/.test(user_account)){
        $("#account_tip").css("display","block");
        flag= false;
    }else {
        $("#account_tip").css("display","none");

    }
    if(!/[\S]{1,20}/.test(user_name)){
        $("#name_tip").css("display","block");
        flag= false;
    }else {
        $("#name_tip").css("display","none");

    }
    if(!/[\S]{1,20}/.test(user_nickname)){
        $("#nickname_tip").css("display","block");
        flag= false;
    }else {
        $("#nickname_tip").css("display","none");

    }
    if(!/\w{6,16}$/.test(user_password)){
        $("#password_tip").css("display","block");
        flag= false;
    }else {
        $("#password_tip").css("display","none");

    }
    if(confirm_password!=user_password){
        $("#confirm_password_tip").css("display","block");
        flag= false;
    }else {
        $("#confirm_password_tip").css("display","none");

    }
    return  flag;
}
var isAccount_enable=true;
var isCode_right=false;
$(function () {
    $("#user_account").bind("blur",()=>{
        let account=$("#user_account").val();
        if(account=="" ||account==null)
            return;

        if(!/^[A-z]\w{5,16}$/.test(account)){
            $("#account_tip").css("display","block");
            return;
        }else {
            $("#account_tip").css("display","none");
        }

        $.post("/checkAccount",
            {
                account:$("#user_account").val()
            },(res)=>{
            res=JSON.parse(res)
                if(res.status==1){
                    $("#account_enable_tip_yes").show()
                    $("#account_enable_tip_no").hide()
                    isAccount_enable=true;
                }else {
                    $("#account_enable_tip_yes").hide()
                    $("#account_enable_tip_no").show()
                    isAccount_enable=false;
                }
            })
    })


    $("#code").bind("blur",()=>{
        $.post("/verifyImageCode",{
                code:$("#code").val()
            },
            (res)=>{
                res=JSON.parse(res)
                if(res==0){
                    isCode_right=false;
                    $("#code_tip").show();
                }else {
                    isCode_right=true;
                    $("#code_tip").hide();
                }
            })
    })
})
function toload() {
    window.location.href="/jsp/login.jsp";
}
function do_register() {

    if(!verifyFormData())
        return;

    if(!isAccount_enable){
        $("#user_account").focus();
        return;
    }
    if(!isCode_right){
        $("#code").focus()
        return;
    }
    $.post("/addUser",
        {user:JSON.stringify(getUserObj())},
        (res)=>{
            if(res>0){
               console.log("注册成功");
                window.location.href="/jsp/login.jsp"

            }else {
                console.log("注册失败");
                alert("注册失败，请重试！")
            }
        })

}