
    $(function () {
    getDataFromBack("/home","","GET",(data)=>{
        // data=JSON.parse(data);
        console.log(data);
        $.each(data,(i,v)=>{
            console.log(v)
            loadNewsInfo(data.news);
            loadMessagesInfo(data.messages);
            loadCompany(data.company)
        })
    })
    $.getScript("/js/top.js",(res)=>{
        console.log("载入成功:"+res)
    });
    // $.getScript("/js/bottom.js",(res)=>{
    //     console.log("载入成功:"+res)
    // })
})

function loadNewsInfo(data) {
    let htmlStr="";
    $.each(data,(i,news)=>{
        htmlStr+=`
        <div>
            <span class="iconfont icon-jiahao"></span>
            <span>${news.title}</span>
            <span><fmt:formatDate value="${news.pubTime}" type="date"/></span>
        </div>
        `
    })
    $("#news-menu").html(htmlStr)
}
function loadMessagesInfo(data) {
    let htmlStr="";
    $.each(data,(i,msg)=>{
        htmlStr+=`
         <div>
                        <div style="background: url('${URL}${msg.photo}');background-size: 100% 100%"></div>
                        <div>
                            <div>
                                <p>${msg.firstName}先生：${msg.husMessage}</p>
                                <p>${msg.firstName}太太：${msg.wifeMessage}</p>
                            </div>
                        </div>
                    </div>
        `
    })
    $("#appreciation").html(htmlStr)
}
function loadCompany(data) {
    $("#company_name").html(data.name)
    $("#company_introduce").html(data.introduce)
}